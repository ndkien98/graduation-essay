import {Component, OnInit} from '@angular/core';
import {DataserviecsService} from '../services/dataservices/dataserviecs.service';
import {CategoryService} from '../services/categoryservice/category.service';
import {Categories} from '../shared/models/category';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(private data: DataserviecsService, private categoryService: CategoryService) { }
  name: string;
  category: Array<Categories>;
  // get data from search box
  inputValue: string;
  inputCate = 'all';
  ngOnInit(): void {
    this.data.currentMessage.subscribe((message) => this.name = message);
    // this.category = new Array<any>();
    this.categoryService.getAllCategories().subscribe( (categories) => {
      this.category = categories;
      console.log(this.category);
      for (let i = 0; i < this.category.length; i++) {
        if (i > 6) {
          this.category[i].className = 'cat-link-orther';
          console.log(this.category[i].categoryName);
        }
        else {
          this.category[i].className = '';
        }
      }
    });
    // this.category = [{
    //   name: 'Android',
    //   classname: '',
    // },
    //   {name: 'iOS',
    //     classname: ''
    //   }, {name: 'Windows phone',
    //     classname: ''
    //   },
    //   {name: 'PHP & MySQL',
    //     classname: ''
    //   },
    //   {name: 'WordPress',
    //     classname: ''
    //   },
    //   {name: 'Joomla',
    //     classname: ''
    //   },
    //   {name: 'Visual C#',
    //     classname: ''
    //   },
    //   {name: 'Asp/Asp.Net',
    //     classname: ''
    //   },
    //   {name: 'Java/JSP',
    //     classname: ''
    //   },
    //   {name: 'Visual Basic',
    //     classname: ''
    //   },
    //   {name: 'Cocos2D',
    //     classname: 'cat-link-orther'
    //   },
    //   {name: 'Unity',
    //     classname: 'cat-link-orther'
    //   },
    //   {name: 'Visual C++',
    //     classname: 'cat-link-orther'
    //   },
    //   {name: 'Html & Template',
    //     classname: 'cat-link-orther'
    //   },
    //   {name: 'Java/JSP',
    //     classname: 'cat-link-orther'
    //   }];
  }
  onCate(event): void {
    this.inputCate = event.target.value;
    console.log(this.inputCate);
  }
  onKey(event): void {
    this.inputValue = event.target.value;
    console.log(this.inputValue);
  }
}
