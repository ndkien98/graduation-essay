import {Component, OnInit} from '@angular/core';
import {Courses} from '../../../../shared/_models/courses';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {DES_LOAD_DATA_FORM_SERVER} from "../../../../shared/_models/constant";

@Component({
  selector: 'app-detail-courses',
  templateUrl: './detail-courses.component.html',
  styleUrls: ['./detail-courses.component.css']
})
export class DetailCoursesComponent implements OnInit {
  courses: Courses;

  constructor(
    public bsModalRef: BsModalRef,
  ) {
  }
  ngOnInit() {
    if (!this.courses) {
      this.bsModalRef.hide();
      alert(DES_LOAD_DATA_FORM_SERVER);
      window.location.reload();
    }
  }

}
