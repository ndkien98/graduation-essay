import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {Select2OptionData} from 'ng-select2';
import {Options} from 'select2';
import {
  DataConvertSelect2, DES_LOAD_DATA_FORM_SERVER,
  ID_ROLE_LECTURE,
  ID_ROLE_STUDENT,
  SUCCESS
} from '../../../../shared/_models/constant';
import {DepartmentService} from '../../../../shared/_service/department.service';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Lecturer, Student} from '../../../../shared/_models/user';
import {UserService} from '../../../../shared/_service/user.service';
import {BaseService} from "../../../../shared/_service/base.service";
import {AuthenticationService} from "../../../../shared/_service/authentication.service";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  public dataForSelect2Role: Array<Select2OptionData>;
  public dataForSelect2Department: Array<Select2OptionData>;
  public options: Options;
  public dataConvert: DataConvertSelect2;

  userFormGroup: FormGroup;

  public onClose: Subject<any>;

  date: Date;
  roleId: any;
  departments: any;
  gender: any;
  STUDENT = ID_ROLE_STUDENT;
  LECTURE = ID_ROLE_LECTURE;

  constructor(
    private formBuilder: FormBuilder,
    private departemtnService: DepartmentService,
    public bsModalRef: BsModalRef,
    private userService: UserService,
    private authentication: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    BaseService.checkNullOrUndefine(this.roleId);
    this.date = new Date();
    this.setOptionSelect2();
    this.onClose = new Subject();
    this.createForm();
  }


  setOptionSelect2() {
    // set option for role
    this.options = {
      theme: 'classic',
      width: '100%',
    };
    // this.dataForSelect2Role = [
    //   {
    //     id: this.STUDENT + '',
    //     text: 'Sinh viên'
    //   },
    //   {
    //     id: this.LECTURE + '',
    //     text: 'Giảng viên'
    //   }
    // ];
    const datas = [];
    // set option for department
    this.departemtnService.getAllDepartment().subscribe((data: any) => {
        this.departments = data;
        data.map(object => {
          this.dataConvert = new DataConvertSelect2(object.departmentCode, object.departmentName);
          datas.push(this.dataConvert);
        });
        this.dataForSelect2Department = datas;
      }, error1 => {
        this.bsModalRef.hide();
        alert(DES_LOAD_DATA_FORM_SERVER);
      }
    );
  }

  createForm() {
    if (this.roleId == this.LECTURE) {
      this.userFormGroup = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        fullname: ['', Validators.required],
        birthDate: [''],
        emailAddress: [''],
        phoneNumber: [''],
        // roleId: ['', Validators.required],
        address: ['', Validators.required]
      });
    } else if (this.roleId == this.STUDENT) {
      this.userFormGroup = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        fullname: ['', Validators.required],
        birthDate: [''],
        emailAddress: [''],
        phoneNumber: [''],
        // roleId: ['', Validators.required],
        address: ['', Validators.required]
      });
    }
  }
  //
  // onChangeRole(role: any) {
  //   if (role == this.STUDENT + '') {
  //     this.checkSelectRole = this.STUDENT;
  //   } else if (role == this.LECTURE + '') {
  //     this.checkSelectRole = this.LECTURE;
  //   }
  //   console.log(this.checkSelectRole);
  //   this.userFormGroup.patchValue({
  //     username: '',
  //     address: ''
  //   });
  // }

  onChangeGender(event: Event) {
    this.gender = (event.target as Element).getAttribute('value');
  }

  onSubmit() {
    if (this.roleId.toString() == ID_ROLE_LECTURE.toString()) {
      // @ts-ignore
      const lecturer = new Lecturer();
      lecturer.roleId = this.roleId;
      lecturer.username = this.userFormGroup.controls.username.value;
      lecturer.password = this.userFormGroup.controls.password.value;
      lecturer.fullName = this.userFormGroup.controls.fullname.value;
      lecturer.birthDate = this.userFormGroup.controls.birthDate.value;
      lecturer.gender = this.gender;
      lecturer.createdDate = this.date.getFullYear() + '-' + this.date.getMonth() + '-' + this.date.getDate();
      lecturer.createdBy = this.authentication.getCurrentUser.username;
      lecturer.birthDate = this.userFormGroup.controls.birthDate.value;
      lecturer.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      lecturer.departmentCode = this.userFormGroup.controls.address.value;
      lecturer.emailAddress = this.userFormGroup.controls.emailAddress.value;
      lecturer.status = 1;
      console.log(lecturer);
      this.userService.addLecture(lecturer).subscribe((data: any) => {
          this.onClose.next(SUCCESS);
          this.bsModalRef.hide();
        }, error1 => {
          alert(DES_LOAD_DATA_FORM_SERVER);
        }
      );
    } else if (this.roleId.toString() == ID_ROLE_STUDENT) {
      // @ts-ignore
      const student = new Student();
      student.roleId = this.roleId;
      student.username = this.userFormGroup.controls.username.value;
      student.password = this.userFormGroup.controls.password.value;
      student.fullName = this.userFormGroup.controls.fullname.value;
      student.birthDate = this.userFormGroup.controls.birthDate.value;
      student.gender = this.gender;
      student.createdDate = this.date.getFullYear() + '-' + this.date.getMonth() + '-' + this.date.getDate();
      student.createdBy = this.authentication.getCurrentUser.username;
      student.birthDate = this.userFormGroup.controls.birthDate.value;
      student.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      student.classCode = this.userFormGroup.controls.address.value;
      student.emailAddress = this.userFormGroup.controls.emailAddress.value;
      student.status = 1;
      this.userService.addStudent(student).subscribe((data: any) => {
          this.onClose.next(SUCCESS);
          this.bsModalRef.hide();
        }, error1 => {
          alert(DES_LOAD_DATA_FORM_SERVER);
        }
      );
    } else {
      alert('Lỗi hệ thống, đề nghị tải lại!');
      this.bsModalRef.hide();
    }
  }

}
