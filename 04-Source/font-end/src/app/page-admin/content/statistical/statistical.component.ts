import {Component, OnInit} from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import {CategoriesService} from '../../../shared/_service/categories.service';
import {Subject} from 'rxjs';
import {SubjectsService} from '../../../shared/_service/subjects.service';
import {YearsSemesterService} from '../../../shared/_service/years-semester.service';
import {DataConvertSelect2} from '../../../shared/_models/constant';
import {Select2OptionData} from 'ng-select2';
import {Options} from 'select2';

@Component({
  selector: 'app-statistical',
  templateUrl: './statistical.component.html',
  styleUrls: ['./statistical.component.css']
})

export class StatisticalComponent implements OnInit {

  constructor(
              private  categoriesService: CategoriesService,
              private  subjectsService: SubjectsService,
              private yearsSemesterService: YearsSemesterService,
                ) {
  }
  public doughnutChartData: MultiDataSet = [
    [200, 450, 100],
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLabels: Label[];
  public doughnutChartLabelsSubject: Label[];

  // chart bar
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: any = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 100, 81, 56, 55, 40], label: 'Series A' },
  ];

  dtTrigger = new Subject();
  listCategories: any = [];
  subjects: any = [];
  listYear: any = [];
  dataConvert: DataConvertSelect2;
  dataSelect2StartYears: Array<Select2OptionData>;
  dataSelect2FinishYears: Array<Select2OptionData>;
  option: Options;
  ngOnInit() {
    this.loadAllCategories();
    this.doughnutChartLabels = this.listCategories;
    this.loadSujects();
    this.doughnutChartLabelsSubject = this.subjects;
    this.loadAllYearsSemester();
    this.setAllYearsForSelect2();
  }

  private loadAllCategories() {
    return this.categoriesService.getAllCategories().subscribe((data: any) => {
      data.map(object => {
        this.listCategories.push(object.categoryName);
      });
      console.log(this.listCategories);
      this.dtTrigger.next();
    });
  }

  private loadSujects() {
    return this.subjectsService.getSubjects().subscribe((data: any) => {
      data.map(object => {
        this.subjects.push(object.subjectName);
      });
      this.dtTrigger.next();
      }, error1 => alert('Lỗi load serve, đề f5 lại trang web')
    );
  }

  loadAllYearsSemester() {
    this.yearsSemesterService.getAllYearsSemester().subscribe((data: any) => {
      data.map(obj => {
        // tslint:disable-next-line:triple-equals
        if ( obj.semester == 1 ) {
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = obj.id;
          this.dataConvert.text = obj.year;
          this.listYear.push(this.dataConvert);
          this.barChartLabels.push(obj.year + ' - ' + ++obj.year);
        }
      });
      this.dataSelect2StartYears = this.listYear;
      this.listYear = [];
      console.log(this.dataSelect2StartYears);
      console.log(this.listYear);
      this.dtTrigger.next();
    });
  }

  setAllYearsForSelect2() {
     this.option = {
      theme: 'classic',
      width: '100%',
      placeholder: 'Chọn năm học',
      dropdownAutoWidth: true,
    };

     // this.dataSelect2Years = this.listYear;
     // console.log(this.dataSelect2Years);
  }

  setFinishYears(id: number) {
    let dem: number;
    // tslint:disable-next-line:prefer-const
    let year: string;
    this.dataSelect2FinishYears = [];
    this.listYear = [];
    if (id !== undefined) {
      this.dataSelect2StartYears.map(obj => {
        dem = Number(obj.id);
        year = obj.text;
        if (Number(obj.id) > id) {
          dem = Number(obj.id);
          year = obj.text;
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = obj.id;
          this.dataConvert.text = obj.text;
          this.listYear.push(this.dataConvert);
        }
      });
      console.log(this.listYear);
      this.dataConvert = new DataConvertSelect2();
      this.dataConvert.id = ++dem;
      this.dataConvert.text = Number(year) + 1;
      console.log(year);
      this.listYear.push(this.dataConvert);
      this.dataSelect2FinishYears = this.listYear;
    }
  }
}
