import { Component, OnInit } from '@angular/core';
import { DataserviecsService } from '../../services/dataservices/dataserviecs.service';
import { ActivatedRoute } from '@angular/router';
import {ProjectService} from '../../services/projectserviecs/projectservice.service';
import {Projects} from '../../shared/models/projects';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  message: string;
  projectData: Projects;
  aaa = 'https://drive.google.com/uc?export=view&id=';
  // tslint:disable-next-line:max-line-length
  constructor(private data: DataserviecsService, private route: ActivatedRoute, private projectService: ProjectService) { }
  ngOnInit(): void {
    this.data.changeMessage('detail');
    this.route.paramMap.subscribe(params => {
      this.projectService.getProjectByName(params.get('nameProject')).subscribe( projectDetail => {
        this.projectData = projectDetail;
        console.log(this.projectData);
      });
      console.log(params.get('nameProject'));
    });
  }
}
