create table years_semesters
(
    id           int auto_increment
        primary key,
    year         int  not null,
    semester     int  not null,
    start_date   date null,
    weeks_number int  null,
    constraint year
        unique (year, semester)
)
    collate = utf8mb4_general_ci;

INSERT INTO projects_database2.years_semesters (id, year, semester, start_date, weeks_number) VALUES (2, 2020, 2, '2021-01-01', 20);
INSERT INTO projects_database2.years_semesters (id, year, semester, start_date, weeks_number) VALUES (3, 2018, 1, '2018-09-05', 21);
INSERT INTO projects_database2.years_semesters (id, year, semester, start_date, weeks_number) VALUES (4, 2017, 2, '2017-01-01', 21);
INSERT INTO projects_database2.years_semesters (id, year, semester, start_date, weeks_number) VALUES (5, 2019, 1, '2019-12-15', 20);
INSERT INTO projects_database2.years_semesters (id, year, semester, start_date, weeks_number) VALUES (6, 2019, 2, '2020-09-09', 20);
INSERT INTO projects_database2.years_semesters (id, year, semester, start_date, weeks_number) VALUES (7, 2020, 1, '2020-07-22', 20);
INSERT INTO projects_database2.years_semesters (id, year, semester, start_date, weeks_number) VALUES (8, 2021, 1, '2021-10-10', 20);