create table lecturers
(
    lecturer_code   varchar(10) not null
        primary key,
    department_code varchar(5)  null,
    constraint lecturers_ibfk_1
        foreign key (lecturer_code) references users (username),
    constraint lecturers_ibfk_2
        foreign key (department_code) references departments (department_code)
)
    collate = utf8mb4_general_ci;

create index department_code
    on lecturers (department_code);

create index lecturer_code
    on lecturers (lecturer_code);

INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('GV01', 'TH01');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('CNP07', 'TH02');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('CNP08', 'TH02');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('CNP10', 'TH02');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('CNP13', 'TH02');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('CNP21', 'TH02');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('GV02', 'TH02');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('GV03', 'TH02');
INSERT INTO projects_database2.lecturers (lecturer_code, department_code) VALUES ('GV19', 'TH02');