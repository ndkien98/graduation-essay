(function($) {
  $(window).scroll(function () {
    var width = $(window).width();
    $('#nav-top-menu').removeClass('nav-ontop');
    if ($('header').hasClass('home')) {
      $('#nav-top-menu').find('.vertical-menu-content').removeAttr('style');
      if (width > 1024)
        $('#nav-top-menu').find('.vertical-menu-content').show();
      else {
        $('#nav-top-menu').find('.vertical-menu-content').hide();
      }
      $('#nav-top-menu').find('.vertical-menu-content').removeAttr('style');
    }
  })
  $(document).on('click','.open-cate',function(){
    $(this).closest('.vertical-menu-content').find('li.cat-link-orther').each(function(){
      $(this).slideDown();
    });
    $(this).addClass('colse-cate').removeClass('open-cate').html('Đóng');
  })
  /* Close category */
  $(document).on('click','.colse-cate',function(){
    $(this).closest('.vertical-menu-content').find('li.cat-link-orther').each(function(){
      $(this).slideUp();
    });
    $(this).addClass('open-cate').removeClass('colse-cate').html('Xem tất cả');
    return false;
  })
  /* Open menu on mobile */
  $(document).on('click','.btn-open-mobile',function(){
    var width = $(window).width();
    if(width >1024){
      if($('header').hasClass('home')){
        if($('#nav-top-menu').hasClass('nav-ontop')){
        }else{
          return false;
        }
      }
    }
    $(this).closest('.box-vertical-megamenus').find('.vertical-menu-content').slideToggle();
    $(this).closest('.title').toggleClass('active');
    return false;
  })
})(jQuery);
