import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../shared/_service/authentication.service';
import {BaseService} from "../../shared/_service/base.service";
import {User} from "../../shared/_models/user";
import {
  ID_ROLE_LECTURE,
  ID_ROLE_STUDENT,
  routerCategory,
  routerCourses,
  routerDecentralization,
  routerDepartment,
  routerHome,
  routerProject,
  routerProjectAdd,
  routerProjectDetail, routerProjectEdit, routerRevise,
  routerStatistical,
  routerSubjects, routerUser,
  routerYearsSemesters
} from "../../shared/_models/constant";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  status: boolean;
  load: boolean;
  user: User;
  constructor(
    private builder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.status = false;
    this.createForm();
  }

  private createForm() {
    this.loginForm = this.builder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  public submit() {
    if (this.loginForm.status == 'INVALID') {
      this.status = true;
    } else {
      this.authenticationService.login(this.loginForm.value).subscribe((data: any) => {
          this.user = data;
          if (this.user.roleId == ID_ROLE_STUDENT) {
            this.router.navigateByUrl(routerProject);
          } else if (this.user.roleId == ID_ROLE_LECTURE) {
            this.router.navigateByUrl(routerHome);
          } else {
          this.router.navigateByUrl(routerHome);
          }
        },
        error1 => {
          alert('Username hoặc password không đúng');
        }
      );
    }
  }
}
