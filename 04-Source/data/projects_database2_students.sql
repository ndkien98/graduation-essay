create table students
(
    student_code varchar(10) not null
        primary key,
    class_code   varchar(10) null,
    constraint students_ibfk_1
        foreign key (student_code) references users (username)
)
    collate = utf8mb4_general_ci;

create index student_code
    on students (student_code);

INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611184', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611234', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611235', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611244', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611256', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611283', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611289', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611299', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611981', 'K61CNTTP');
INSERT INTO projects_database2.students (student_code, class_code) VALUES ('611999', 'K61CNTTP');