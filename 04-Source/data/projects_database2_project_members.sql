create table project_members
(
    id           int auto_increment
        primary key,
    student_code varchar(10)                 null,
    full_name    varchar(50) charset utf8mb4 null,
    class_code   varchar(10)                 null,
    project_code varchar(10)                 null,
    constraint project_members_ibfk_1
        foreign key (project_code) references projects (project_code)
)
    collate = utf8mb4_general_ci;

create index project_code
    on project_members (project_code);

INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (47, '611389', 'Hoàng Bình Dương', 'K61CNTTP', 'P03');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (52, '611244', 'Nguyễn Tuấn Mạnh', 'K61CNTTP', 'P04');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (53, '611980', 'Nguyễn Thị Hà', 'K61CNTTP', 'P05');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (54, '612358', 'Nguyễn Thị Nhung', 'K61CNTTP', 'P05');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (55, '611292', 'Nguyễn Đắc kiên', 'K61CNTTP', 'P02');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (56, '611289', 'Nguyễn Văn Dần', 'K61CNTTP', 'P01');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (57, '611345', 'Nguyễn Tuấn Mạnh', 'K61CNTTP', 'P01');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (60, '611292', 'Nguyễn Đắc Kiên', 'K61CNTTP', 'P0001');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (61, '611293', 'Nguyễn Văn Thanh', 'k61CNTTC', 'P0001');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (62, '611345', 'Nguyễn Văn Mạnh', 'K63CNTT', 'P00078');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (63, '611123', 'Nguyễn Thị Thu', 'K64CNTTC', 'P00078');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (67, '611234', 'Nguyễn Hạ Huy', 'K61CNTTP', 'P000012');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (68, '611292', 'Dương Hồng Phong', 'K64CNPM', 'P000012');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (69, '611256', 'Nguyễn Thị Huyền ', 'K64CNPM', 'P000012');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (72, '611292', 'Nguyễn Đắc Kiên', 'K63CNTTP', 'P00030');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (73, '611345', 'Nguyễn Tuấn Anh ', 'K63CNTTp ', 'P00030');
INSERT INTO projects_database2.project_members (id, student_code, full_name, class_code, project_code) VALUES (75, '611235', 'Nguyễn Văn Duy ', 'K61CNTTP', 'P000289');