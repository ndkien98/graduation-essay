import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataserviecsService {
  private messageSource = new BehaviorSubject<string>('home');
  currentMessage = this.messageSource.asObservable();
  constructor() { }

  // tslint:disable-next-line:typedef
  changeMessage(message: string) {
    this.messageSource.next(message);
  }
}
