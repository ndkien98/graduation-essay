package com.fita.project.services.impl;

import com.fita.project.dto.CourseDTO;
import com.fita.project.dto.ProjectDTO;
import com.fita.project.services.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Service
public class MailServiceImpl implements MailService {

    private static final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);

    private final JavaMailSender emailSender;

    public MailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public void sendMail(String emailResponsiblePerson, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailResponsiblePerson);
        message.setSubject(subject);
        message.setText(content);
        this.emailSender.send(message);
        log.info("sent mail to person");
    }

    public String sendHtmlEmail(String username,String password,String emailResponsiblePerson) throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true, "text/plain; charset=UTF-8");

        String htmlMsg = "<h2> Bạn đã được cấp tài khoản đăng nhập vào hệ thống trưng bày đồ án môn học </h2>" +
                "<h3> " + String.format("tên tài khoản: %s, mật khẩu: %s",username,password) + "</h3>"
                +"<img src='https://fita.vnua.edu.vn/wp-content/uploads/2021/01/banner_WEB_Tuyen-sinh4-960x220.png'>";

        message.setContent(htmlMsg, "text/html; charset=UTF-8");

        helper.setTo(emailResponsiblePerson);
        helper.setSubject("Display system of subject projects");

        this.emailSender.send(message);
        return "Email Sent!";
    }

    @Override
    public void sendMailAddProject(CourseDTO courseDTO, ProjectDTO projectDTO, String emailLecture) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true, "text/plain; charset=UTF-8");

        String htmlMsg = "<h2> Bạn đã được thêm vào 1 đồ án của sinh viên </h2>" +
                "<h3> " + String.format("Mã đồ án: %s",projectDTO.getProjectCode()) +  "</h3>"
                + "<h3> " + String.format("Tên đồ án: %s",projectDTO.getProjectName()) +  "</h3>"
                + "<h3> " + String.format("Mã sinh viên: %s",projectDTO.getStudentCode()) +  "</h3>"
                + "<h3> " + String.format("Mã môn học: %s",courseDTO.getSubjectCode()) +  "</h3>"
                + "<h3> " + String.format("Tên môn học: %s",courseDTO.getSubjectName()) +  "</h3>"
                + "<h3> " + String.format("Nhóm môn học: %d",courseDTO.getSubjectGroup()) +  "</h3>"
                + "<h3> " + String.format("Học kỳ/ Năm học: %s",courseDTO.getSemester() + "/" + courseDTO.getYear() + "-" + courseDTO.getYear() + 1) +  "</h3>"
                +"<img src='http://fita.vnua.edu.vn/wp-content/uploads/2014/06/logo-vi.png'>";

        message.setContent(htmlMsg, "text/html; charset=UTF-8");

        helper.setTo(emailLecture);
        helper.setSubject("Display system of subject projects");

        this.emailSender.send(message);
    }

    public void sendMailEditProject(ProjectDTO projectDTO, String studentEmail) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true, "text/plain; charset=UTF-8");

        String htmlMsg = "<h2> Bạn đã được duyệt một đồ án </h2>" +
                "<h3> " + String.format("Mã đồ án: %s",projectDTO.getProjectCode()) +  "</h3>"
                + "<h3> " + String.format("Tên đồ án: %s",projectDTO.getProjectName()) +  "</h3>"
                + "<h3> " + String.format("Mã sinh viên: %s",projectDTO.getStudentCode()) +  "</h3>"
                + "<h3> " + String.format("Mã môn học: %s",projectDTO.getSubjectCode()) +  "</h3>"
                + "<h3> " + String.format("Tên môn học: %s",projectDTO.getSubjectName()) +  "</h3>"
                + "<h3> " + String.format("Nhóm môn học: %d",projectDTO.getSubjectGroup()) +  "</h3>"
                + "<h3> " + String.format("Năm học học kỳ: %s",projectDTO.getYearSemester()) +  "</h3>"
                +"<img src='http://fita.vnua.edu.vn/wp-content/uploads/2014/06/logo-vi.png'>";

        message.setContent(htmlMsg, "text/html; charset=UTF-8");

        helper.setTo(studentEmail);
        helper.setSubject("Display system of subject projects");

        this.emailSender.send(message);
    }
}
