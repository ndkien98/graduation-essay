create table roles_functions
(
    id          int auto_increment
        primary key,
    role_id     int           not null,
    function_id int           not null,
    status      int default 0 not null,
    constraint role_id_function_id
        unique (role_id, function_id),
    constraint roles_functions_ibfk_1
        foreign key (role_id) references roles (id),
    constraint roles_functions_ibfk_2
        foreign key (function_id) references functions (id)
)
    collate = utf8mb4_general_ci;

create index function_id
    on roles_functions (function_id);

create index role_id
    on roles_functions (role_id);

INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (1, 1, 1, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (2, 1, 2, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (3, 1, 3, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (4, 1, 4, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (5, 1, 5, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (6, 1, 6, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (7, 1, 7, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (8, 1, 8, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (9, 1, 9, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (10, 1, 10, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (11, 1, 11, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (12, 1, 12, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (13, 1, 13, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (14, 1, 14, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (15, 1, 15, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (16, 1, 16, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (17, 1, 17, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (18, 1, 18, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (19, 1, 19, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (20, 1, 20, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (21, 1, 21, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (22, 1, 22, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (23, 1, 23, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (24, 1, 24, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (25, 1, 25, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (26, 1, 26, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (27, 1, 27, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (28, 1, 28, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (29, 1, 29, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (30, 1, 30, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (31, 1, 31, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (32, 1, 32, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (33, 1, 33, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (34, 1, 34, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (35, 1, 35, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (36, 1, 36, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (37, 1, 37, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (38, 2, 1, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (39, 2, 2, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (40, 2, 3, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (41, 2, 4, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (42, 2, 5, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (43, 2, 6, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (44, 2, 7, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (45, 2, 8, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (46, 2, 9, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (47, 2, 10, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (48, 2, 11, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (49, 2, 12, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (50, 2, 13, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (51, 2, 14, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (52, 2, 15, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (53, 2, 16, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (54, 2, 17, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (55, 2, 18, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (56, 2, 19, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (57, 2, 20, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (58, 2, 21, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (59, 2, 22, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (60, 2, 23, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (61, 2, 24, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (62, 2, 25, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (63, 2, 26, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (64, 2, 27, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (65, 2, 28, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (66, 2, 29, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (67, 2, 30, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (68, 2, 31, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (69, 2, 32, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (70, 2, 33, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (71, 2, 34, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (72, 2, 35, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (73, 2, 36, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (74, 2, 37, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (75, 3, 1, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (76, 3, 2, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (77, 3, 3, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (78, 3, 4, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (79, 3, 5, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (80, 3, 6, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (81, 3, 7, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (82, 3, 8, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (83, 3, 9, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (84, 3, 10, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (85, 3, 11, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (86, 3, 12, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (87, 3, 13, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (88, 3, 14, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (89, 3, 15, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (90, 3, 16, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (91, 3, 17, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (92, 3, 18, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (93, 3, 19, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (94, 3, 20, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (95, 3, 21, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (96, 3, 22, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (97, 3, 23, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (98, 3, 24, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (99, 3, 25, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (100, 3, 26, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (101, 3, 27, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (102, 3, 28, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (103, 3, 29, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (104, 3, 30, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (105, 3, 31, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (106, 3, 32, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (107, 3, 33, 1);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (108, 3, 34, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (109, 3, 35, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (110, 3, 36, 0);
INSERT INTO projects_database2.roles_functions (id, role_id, function_id, status) VALUES (111, 3, 37, 0);