import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {baseUrl} from '../../shared/constant';
import {Projects} from '../../shared/models/projects';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }
  getAllProject(): Observable<any> {
    const url = baseUrl + 'projects/get-all';
    return this.http.get<Projects>(url);
  }

  getProjectByName(name: string): Observable<any> {
    const url = baseUrl + 'projects/get-by-project-code/' + name;
    return this.http.get<Projects>(url);
  }

  getProjectById(categoryCode: string): Observable<any>{
    const url = baseUrl + 'projects/get-by-category-code/' + categoryCode;
    return this.http.get<Projects>(url);
  }

  findProjectByKey(nProject: string, nCategory: string): Observable<any> {
    const url = baseUrl + 'projects/find-by-key?nProject=' + nProject + '&nCategory=' + nCategory;
    return this.http.get<Projects>(url);
  }
  findProjectByName(nProject: string): Observable<any> {
    const url = baseUrl + 'projects/find-by-name?key=' + nProject;
    return this.http.get<Projects>(url);
  }
}
