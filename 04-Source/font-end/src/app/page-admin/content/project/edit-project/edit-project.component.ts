import { Component, OnInit } from '@angular/core';
import {CoursesService} from "../../../../shared/_service/courses.service";
import {UserService} from "../../../../shared/_service/user.service";
import {HttpClient} from "@angular/common/http";
import {baseUrl} from "../../../../shared/_models/constant";

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit {
  courses: any[];
  users: any[];
  lectures: any[];
  students: any[];
  constructor(
    private coursesService: CoursesService,
    private userService: UserService,
    private http: HttpClient
  ) {}
  ngOnInit(): void {
    this.getListUser();
    // this.getListStudent();
    // this.getListLecture();
  }
  getListLecture() {
    this.userService.getLectures().subscribe((data: any) => {
      this.lectures = data;
      console.log(this.lectures);
    });
  }
  getListStudent() {
   this.http.get(baseUrl + 'api/users/get-students').subscribe((data: any) => {
     this.users = data;
     console.log(this.users);
   });
  }
  getListUser() {
    this.userService.getUsers().subscribe((data: any) => {
      this.users = data;
      console.log(this.users);
    });
  }
}
