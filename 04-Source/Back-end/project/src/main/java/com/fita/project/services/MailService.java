package com.fita.project.services;

import com.fita.project.dto.CourseDTO;
import com.fita.project.dto.ProjectDTO;

import javax.mail.MessagingException;

public interface MailService {
    void sendMail(String emailResponsiblePerson, String subject, String content);

    String sendHtmlEmail(String username,String password,String emailResponsiblePerson) throws MessagingException;

    void sendMailAddProject(CourseDTO courseDTO, ProjectDTO projectDTO, String emailLecture) throws MessagingException;

    void sendMailEditProject(ProjectDTO projectDTO, String studentEmail) throws MessagingException;
}
