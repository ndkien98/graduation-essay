import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {baseUrl} from '../../shared/constant';
import {Subject} from '../../shared/models/subject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(private http: HttpClient) { }
  getSubjectBySubjectCode(code: string): Observable<any> {
    const url = baseUrl + 'subjects/get-by-subjectcode/' + code;
    return this.http.get<Subject>(url);
  }
}
