create table courses
(
    id                 int auto_increment
        primary key,
    subject_code       varchar(10)                        not null,
    subject_group      int                                not null,
    class_code         varchar(50) charset utf8mb4        null,
    year_semester_id   int                                null,
    lecturer_code      varchar(10)                        null,
    created_date       datetime default CURRENT_TIMESTAMP null,
    created_by         varchar(10)                        null,
    last_modified_date datetime default CURRENT_TIMESTAMP null,
    constraint courses
        unique (subject_code, subject_group, class_code, year_semester_id),
    constraint courses_ibfk_1
        foreign key (subject_code) references subjects (subject_code),
    constraint courses_ibfk_2
        foreign key (lecturer_code) references lecturers (lecturer_code),
    constraint courses_ibfk_3
        foreign key (year_semester_id) references years_semesters (id)
)
    collate = utf8mb4_general_ci;

create index lecturer_code
    on courses (lecturer_code);

create index subject_code
    on courses (subject_code);

create index year_semester_id
    on courses (year_semester_id);

INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (8, 'PTH03115', 1, 'K61CNTTP', 5, 'GV01', '2021-03-11 08:44:17', null, '2021-03-11 08:44:17');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (9, 'PTH03116', 1, 'K61CNTTP', 4, 'GV02', '2021-03-11 08:44:37', null, '2021-03-11 08:44:37');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (10, 'PTH03117', 1, 'K62CNTTP', 4, 'GV02', '2021-03-11 08:44:37', null, '2021-03-11 08:44:37');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (11, 'PTH03118', 1, 'K63CNTTP', 4, 'GV02', '2021-03-11 08:44:37', null, '2021-03-11 08:44:37');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (12, 'PTH03117', 1, 'K64CNTTP', 4, 'GV02', '2021-03-11 08:44:37', null, '2021-03-11 08:44:37');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (34, 'PTH03118', 2, 'K61CNTTP', 4, 'CNP07', '2021-07-23 11:58:56', null, '2021-07-23 11:58:56');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (35, 'PTH03117', 3, 'K61CNTTP', 5, 'CNP07', '2021-07-23 12:01:54', null, '2021-07-23 12:01:54');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (36, 'PTH03109', 1, 'K61CNTTF', 7, 'CNP08', '2021-07-23 12:41:40', null, '2021-07-23 12:41:40');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (37, 'PTH03120', 1, '1', 8, 'CNP13', '2021-07-27 08:22:45', null, '2021-07-27 08:22:45');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (38, 'PTH03119', 8, 'K61CNTTP', 2, 'CNP07', '2021-08-06 08:47:12', null, '2021-08-06 08:47:12');
INSERT INTO projects_database2.courses (id, subject_code, subject_group, class_code, year_semester_id, lecturer_code, created_date, created_by, last_modified_date) VALUES (39, 'PTH03100', 1, 'K61CNTTP', 2, 'GV19', '2021-08-06 14:36:52', null, '2021-08-06 14:36:52');