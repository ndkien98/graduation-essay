import {Inject, Injectable} from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})

export class LocalstorageService {
  private idCategory: string;

  constructor(@Inject(LOCAL_STORAGE)private storage: StorageService) {
  }

  public storeSetOnLocal(id: string): void {
    this.idCategory = id;
    this.storage.set('id', this.idCategory);
  }

  public storeGetOnLocal(): string {
    return this.storage.get('id');
  }
}
