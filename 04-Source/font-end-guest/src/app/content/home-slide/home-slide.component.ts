import { Component, OnInit } from '@angular/core';
import {DataserviecsService} from '../../services/dataservices/dataserviecs.service';

@Component({
  selector: 'app-home-slide',
  templateUrl: './home-slide.component.html',
  styleUrls: ['./home-slide.component.css']
})
export class HomeSlideComponent implements OnInit {

  constructor(private data: DataserviecsService) { }

  ngOnInit(): void {
    this.data.changeMessage('home');
  }

}
