import {Component, OnInit, ViewChild} from '@angular/core';
import {Projects} from "../../../shared/_models/projects";
import {DataTableDirective} from "angular-datatables";
import {Subject} from "rxjs";
import {Options} from 'select2';
import {YearsSemesterService} from "../../../shared/_service/years-semester.service";
import {ProjectService} from "../../../shared/_service/proejct.service";
import {Select2OptionData} from "ng-select2";
import {DataConvertSelect2, PROJECT_DISABLE, PROJECT_ENABLE} from "../../../shared/_models/constant";
import {YearsSemester} from "../../../shared/_models/years-semester";
import {Router} from "@angular/router";
import {User} from "../../../shared/_models/user";
import {AuthenticationService} from "../../../shared/_service/authentication.service";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  projects;
  projectOfStudent;
  projectOfStudentDisable;
  projectOfLecture;
  semesterYear: string;


  @ViewChild(DataTableDirective, {static: false}) // khai bao cac tuy chon cua dataTable
  dtElement: DataTableDirective;
  dataTableOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  @ViewChild(DataTableDirective, {static: false}) // khai bao cac tuy chon cua dataTable
  dtElement2: DataTableDirective;
  dataTableOptions2: DataTables.Settings = {};
  dtTrigger2 = new Subject();

  dataSelect2: Array<Select2OptionData>;
  option: Options;
  dataConvert: DataConvertSelect2;
  selected: boolean;
  user: User;



  constructor(
    private projectService: ProjectService,
    private yearsSemesterService: YearsSemesterService,
    private router: Router,
    private authentication: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    this.user = this.authentication.getCurrentUser;
    this.projects = new Array();
    this.projectOfStudent = new Array();
    this.projectOfLecture = new Array();
    this.projectOfStudentDisable = new Array();
    this.loadAllProject();
    this.setAllYearsSemesterForSelect2();
    this.loadProjectDisableStudent();
  }

  private loadAllProject() {
    this.dataTableOptions = {
      pagingType: 'full_numbers'
    };
    return this.projectService.getAllProject().subscribe(
      (data) => {
        data.map(project => {
          console.log('project : ' + project.lecturerCode);
          if (project.status == PROJECT_ENABLE) {
            // this.projects.push(project);
            if (project.studentCode == this.user.username) {
              this.projectOfStudent.push(project);
            }
            if ( project.lecturerCode == this.user.username) {
              this.projectOfLecture.push(project);
            }
          }
          this.projects.push(project);
        });
        this.dtTrigger.next();
      },
      error1 => alert('Lỗi tải data từ server, đề nghị tại lại trang')
    );
  }
  private loadProjectDisableStudent() {
    this.dataTableOptions2 = {
      pagingType: 'full_numbers'
    };
    return this.projectService.getProjectByStatus(0).subscribe(
      (data) => {
        data.map(project => {
          if (project.studentCode == this.user.username) {
            this.projectOfStudentDisable.push(project);
          }
        });
        this.dtTrigger2.next();
      },
      error1 => alert('Lỗi tải data từ server, đề nghị tại lại trang')
    );
  }

  setAllYearsSemesterForSelect2() {
    this.option = {
      theme: 'classic',
      width: '100%',
      placeholder: 'Chọn học kỳ - năm học',
    };
    const dataArray = [];
    // tslint:disable-next-line:prefer-const
    this.yearsSemesterService.getAllYearsSemester().subscribe((data: YearsSemester[]) => {
        data.map(yearsSemes => {
          this.semesterYear = 'Học kỳ ' + yearsSemes.semester + ' - Năm học ' + yearsSemes.year + ' - ' + ++yearsSemes.year;
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = yearsSemes.id;
          this.dataConvert.text = this.semesterYear;
          dataArray.push(this.dataConvert);
        });
        dataArray.push(new DataConvertSelect2('all', 'Tất cả năm học học kỳ'));
        this.dataSelect2 = dataArray;
      },
      error1 => {
        alert('Lỗi tải data từ serve, đề nghị tải lại trang');
      }
    );
  }

  public onChange(id: any) {
    if (id !== undefined) {
      if (id == 'all') {
        this.projects = [];
        this.projectService.getAllProject().subscribe((data: any) => {
          data.map(project => {
            if (project.status == PROJECT_ENABLE) {
              this.projects.push(project);
            }
          });
        });
      } else {
        this.projects = [];
        this.projectService.getProjectByYearSemesterId(id).subscribe((data: any) => {
          // @ts-ignore
          data.map(project => {
            if (project.status == PROJECT_ENABLE) {
              this.projects.push(project);
            }
          });
        });
      }
    }
  }
  public addProject() {
    this.router.navigateByUrl('/management/project/add');
  }
}
