export class Categories {
  id: number;
  categoryCode: string;
  categoryName: string;
  className: string;
}
