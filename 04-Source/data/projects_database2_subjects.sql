create table subjects
(
    id              int auto_increment
        primary key,
    subject_code    varchar(10)                  not null,
    subject_name    varchar(200) charset utf8mb4 not null,
    department_code varchar(5)                   null,
    constraint subject_code
        unique (subject_code),
    constraint subjects_ibfk_1
        foreign key (department_code) references departments (department_code)
)
    collate = utf8mb4_general_ci;

create index department_code
    on subjects (department_code);

INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (1, 'PTH03115', 'Đồ án 1', 'TH02');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (5, 'PTH03116', 'Lập trình nâng cao', 'TH01');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (6, 'PTH03117', 'Đồ họa máy tính', 'TH01');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (7, 'PTH03118', 'Cơ sở dữ liệu', 'TH02');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (8, 'PTH03119', 'Lập trình hướng đối tượng', 'TH03');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (9, 'PTH03120', 'Khai phá dữ liệu', 'TH04');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (10, 'PTH00021', 'Thực tập chuyên nghành', 'TH02');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (11, 'PTH03109', 'Phát triển ứng dụng web', 'TH02');
INSERT INTO projects_database2.subjects (id, subject_code, subject_name, department_code) VALUES (13, 'PTH03100', 'Lập trình web 2', 'TH02');