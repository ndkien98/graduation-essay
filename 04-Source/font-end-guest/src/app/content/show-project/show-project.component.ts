import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataserviecsService } from '../../services/dataservices/dataserviecs.service';
import { ProjectService } from '../../services/projectserviecs/projectservice.service';
import { Projects } from '../../shared/models/projects';
import {LocalstorageService} from '../../services/localstorageservices/localstorage.service';

@Component({
  selector: 'app-show-project',
  templateUrl: './show-project.component.html',
  styleUrls: ['./show-project.component.css']
})
export class ShowProjectComponent implements OnInit {
  name: string;
  select: string;
  projectList: Array<Projects>;
  totalRecords = 0;
  page = 1;
  stateCout = 0;
  // lay ra category de tim project theo category do
  category: string;
  inputSearch: string;
  aaa = 'https://drive.google.com/uc?export=view&id=';
  // tslint:disable-next-line:max-line-length
  constructor(private projectService: ProjectService, private route: ActivatedRoute, private dataService: DataserviecsService, private storageLocal: LocalstorageService) {
  }

  ngOnInit(): void {
    this.name = 'grid';
    this.select = 'selected';
    this.projectList = new Array<Projects>();

    this.route.paramMap.subscribe(params => {
      if (params.get('nameCategory') === null && params.get('nameCategorySearch') === null) {
        this.projectService.getAllProject().subscribe((data) => {
          data.map(obj => {
            this.projectList.push(obj);
            this.totalRecords++;
          });
          console.log(this.projectList);
          console.log(this.totalRecords);
        });
      }
    });

    this.route.paramMap.subscribe(params => {
      if (params.get('nameCategory') !== null) {
        this.category = params.get('nameCategory');
        // this.projectList = [];
        if (history.state.id !== undefined) {
          this.storageLocal.storeSetOnLocal(history.state.id);
          this.projectService.getProjectById(history.state.id).subscribe( data => {
          this.projectList = data;
          this.totalRecords = this.projectList.length;
          console.log(this.totalRecords);
        });
        }
        else {
            this.projectService.getProjectById(this.storageLocal.storeGetOnLocal()).subscribe( data => {
            this.projectList = data;
            this.totalRecords = this.projectList.length;
            console.log(this.totalRecords);
          });
        }
        console.log(history.state.id);
        this.dataService.changeMessage('show project');
        console.log(params.get('nameCategory'));
      }
    });

    this.route.paramMap.subscribe( params => {
      // console.log(params.get('nameCategory'));
      // console.log(params.get('inputSearch'));
      if (params.get('nameCategorySearch') !== null) {
        if (params.get('nameCategorySearch') === 'ĐỒ ÁN') {
          this.category = 'TẤT CẢ ĐỒ ÁN';
          this.inputSearch = params.get('inputSearch');
          // @ts-ignore
          this.projectService.findProjectByName(this.inputSearch).subscribe( data => {
            this.projectList = data;
            this.totalRecords = this.projectList.length;
            console.log(this.totalRecords);
          });
        }
        else {
        this.category = params.get('nameCategorySearch');
        this.inputSearch = params.get('inputSearch');
        this.dataService.changeMessage('search project');
        // @ts-ignore
          this.projectService.findProjectByKey(this.inputSearch, this.category).subscribe( data => {
          this.projectList = data;
          this.totalRecords = this.projectList.length;
          console.log(this.totalRecords);
        });
        console.log(this.category);
        console.log(this.inputSearch);
        }
        this.dataService.changeMessage('Search Project');
      }
      console.log(this.projectList);
    });
  }

  changeViewGrid(): void {
    this.name = 'grid';
    this.select = 'selected';
  }
  changeViewList(): void {
    this.name = 'list';
    this.select = 'selected';
  }

}
