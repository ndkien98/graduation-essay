create table roles
(
    id        int auto_increment
        primary key,
    role_name varchar(50) charset utf8mb4 not null,
    constraint role_name
        unique (role_name)
)
    collate = utf8mb4_general_ci;

INSERT INTO projects_database2.roles (id, role_name) VALUES (1, 'Admin');
INSERT INTO projects_database2.roles (id, role_name) VALUES (2, 'Giảng viên');
INSERT INTO projects_database2.roles (id, role_name) VALUES (3, 'Sinh viên');