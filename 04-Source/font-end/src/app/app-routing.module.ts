import { NgModule } from '@angular/core';
import {Routes, RouterModule, ExtraOptions, PreloadAllModules} from '@angular/router';
import {LoginComponent} from './page-admin/login/login.component';
import {AuthGuard} from './shared/_helpers/auth.guard';

const routes: Routes = [
  {
    path: 'management',
    loadChildren: () => import('./page-admin/content/content.module')
      .then(m => m.ContentModule), canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '', redirectTo: 'management', pathMatch: 'full'
  },
  { path: '**', redirectTo: 'management' ,  pathMatch: 'full'},
];
const config = {
  preloadingStrategy: PreloadAllModules
};
@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
