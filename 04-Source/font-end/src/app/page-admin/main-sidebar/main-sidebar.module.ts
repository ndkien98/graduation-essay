import {NgModule} from '@angular/core';
import {MainSidebarComponent} from './main-sidebar.component';
import {CommonModule} from '@angular/common';
import { PersonalInformationComponent } from './personal-information/personal-information.component';
import {NgxMaskModule} from 'ngx-mask';
import {MobxAngularModule} from 'mobx-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from "@angular/router";
import {NgSelect2Module} from "ng-select2";

@NgModule({
  declarations: [
    MainSidebarComponent,
    PersonalInformationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgSelect2Module,
    ReactiveFormsModule,
  ],
  exports: [
    MainSidebarComponent,
  ]
})
export class MainSidebarModule {}
