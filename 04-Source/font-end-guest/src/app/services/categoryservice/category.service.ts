import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {baseUrl} from '../../shared/constant';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient){ }
  getAllCategories(): Observable<any>{
    const url = baseUrl + 'categories/get-all';
    return this.http.get<any>(url);
  }
}
