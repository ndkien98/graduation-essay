create table functions
(
    id                   int auto_increment
        primary key,
    function_name        varchar(100) charset utf8mb4 not null,
    action_code          varchar(50)                  not null,
    function_description text                         null,
    constraint action_code
        unique (action_code)
)
    collate = utf8mb4_general_ci;

INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (1, 'Quản lý phân quyền', 'ROLE_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (2, 'Thêm quyền', 'ADD_ROLE', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (3, 'Sửa quyền', 'EDIT_ROLE', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (4, 'Xoá quyền', 'DELETE_ROLE', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (5, 'Quản lý bộ môn', 'DEPARTMENT_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (6, 'Thêm bộ môn', 'ADD_DEPARTMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (7, 'Sửa bộ môn', 'EDIT_DEPARTMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (8, 'Xoá bộ môn', 'DELETE_DEPARTMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (9, 'Quản lý người dùng', 'USER_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (10, 'Thêm giảng viên', 'ADD_LECTURER', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (11, 'Sửa giảng viên', 'EDIT_LECTURER', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (12, 'Xoá giảng viên', 'DELETE_LECTURER', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (13, 'Thêm sinh viên', 'ADD_STUDENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (14, 'Sửa sinh viên', 'EDIT_STUDENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (15, 'Xoá sinh viên', 'DELETE_STUDENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (16, 'Quản lý môn học', 'SUBJECT_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (17, 'Thêm môn học', 'ADD_SUBJECT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (18, 'Sửa môn học', 'EDIT_SUBJECT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (19, 'Xoá môn học', 'DELETE_SUBJECT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (20, 'Quản lý năm học - học kỳ', 'YEAR_SEMESTER_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (21, 'Thêm năm học - học kỳ', 'ADD_YEAR_SEMSETER', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (22, 'Sửa năm học - học kỳ', 'EDIT_YEAR_SEMSETER', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (23, 'Xoá năm học - học kỳ', 'DELETE_YEAR_SEMSETER', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (24, 'Quản lý lớp học phần', 'COURSE_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (25, 'Thêm lớp học phần', 'ADD_COURSE', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (26, 'Sửa lớp học phần', 'EDIT_COURSE', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (27, 'Xoá lớp học phần', 'DELETE_COURSE', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (28, 'Quản lý thể loại', 'CATEGORY_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (29, 'Thêm thể loại', 'ADD_CATEGORY', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (30, 'Sửa thể loại', 'EDIT_CATEGORY', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (31, 'Xoá thể loại', 'DELETE_CATEGORY', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (32, 'Quản lý đồ án', 'PROJECT_MANAGEMENT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (33, 'Thêm đồ án', 'ADD_PROJECT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (34, 'Sửa đồ án', 'EDIT_PROJECT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (35, 'Xoá đồ án', 'DELETE_PROJECT', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (36, 'Kiểm duyệt đồ án', 'PROJECT_APPROVE', null);
INSERT INTO projects_database2.functions (id, function_name, action_code, function_description) VALUES (37, 'Thống kê đồ án', 'PROJECT_ANALYZE', null);