import { TestBed } from '@angular/core/testing';

import { DataserviecsService } from './dataserviecs.service';

describe('DataserviecsService', () => {
  let service: DataserviecsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataserviecsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
