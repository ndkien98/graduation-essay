export class Course{
    id: number;
    subjectCode: string;
    subjectName;
    subjectGroup;
    classCode;
    yearSemesterId;
    year;
    semester;
    lecturerCode;
    lecturerName;
    createdDate;
    createdBy;
    lastModifiedDate;
}
