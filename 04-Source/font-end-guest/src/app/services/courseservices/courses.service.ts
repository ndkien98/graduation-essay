import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {baseUrl} from '../../shared/constant';
import {Course} from '../../shared/models/Course';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient) { }

  getCourseById(id: number): Observable<any> {
    const url = baseUrl + 'courses/get-by-id/' + id;
    return this.http.get<Course>(url);
  }
}
