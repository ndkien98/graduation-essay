import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './content/body.component';
import { HomeSlideComponent } from './content/home-slide/home-slide.component';
import { ShowProjectComponent } from './content/show-project/show-project.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule} from '@angular/common/http';
import { DetailComponent } from './content/detail/detail.component';
import { RouterModule} from '@angular/router';
import { SearchComponent } from './content/search/search.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    HomeSlideComponent,
    ShowProjectComponent,
    DetailComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: '',
        component: BodyComponent,
        // children: [
        //
        //   {
        //     path: '',
        //     component: HomeSlideComponent,
        //   },
        //   {
        //     path: 'home',
        //     component: HomeSlideComponent,
        //   },
        // ]
      },
      {
        path: 'home',
        component: BodyComponent,
      },
      {
        path: 'search/:nameCategorySearch/:inputSearch',
        component: ShowProjectComponent,
      },
      {
        path: 'detail/:nameProject',
        component: DetailComponent
      },
      {
        path: 'CategoryProject/:nameCategory',
        component: ShowProjectComponent
      },
    ], {scrollPositionRestoration: 'enabled'}),
    RouterModule.forChild([{
      path: '',
      component: HeaderComponent,
    }]),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
