
import { AbstractControl} from '@angular/forms';
import { ValidatorFn} from '@angular/forms';

export const emailValidator = (): ValidatorFn => {
  return (control: AbstractControl): {[key: string]: string} => {
    const result = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(control.value);
    return result == true ? null : {error: 'Định dạng email chưa đúng'};
  };
};


export const categorycodeVL = (): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: string } => {
    const result = /^[T][L][0-9]{2,}$/.test(control.value);
    return result == true? null : {'error': 'Sai định dạng'};
  };
};

export const studentCodeVL = (): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: string } => {
    const result = /^(5[0-9]|6[0-4])[0-9]{4}$/.test(control.value);
    return result == true? null : {'error': 'Sai định dạng'};
  };
};


export const yearCodeVL = (): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: string } => {
    const result = /^[2][0][0-9]{2}-[2][0][0-9]{2}$/.test(control.value);
    return result == true? null : {'error': "Sai định dạng"};
  };
};
