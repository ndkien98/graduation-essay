import { Component, OnInit } from '@angular/core';
import {DataserviecsService} from '../../services/dataservices/dataserviecs.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private dataService: DataserviecsService) { }

  ngOnInit(): void {
    this.dataService.changeMessage('search');
  }

}
