create table categories
(
    id            int auto_increment
        primary key,
    category_code varchar(10)                 not null,
    category_name varchar(50) charset utf8mb4 not null,
    constraint category_code
        unique (category_code)
)
    collate = utf8mb4_general_ci;

INSERT INTO projects_database2.categories (id, category_code, category_name) VALUES (1, 'TL01', 'Ứng dụng desktop');
INSERT INTO projects_database2.categories (id, category_code, category_name) VALUES (2, 'TL02', 'Ứng dụng web');
INSERT INTO projects_database2.categories (id, category_code, category_name) VALUES (3, 'TL03', 'Ứng dụng mobile');
INSERT INTO projects_database2.categories (id, category_code, category_name) VALUES (4, 'TL04', 'Truyền thông đa phương tiện');