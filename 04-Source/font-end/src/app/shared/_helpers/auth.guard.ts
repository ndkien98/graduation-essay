import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../_service/authentication.service';
import {
  ID_ROLE_ADMIN,
  ID_ROLE_LECTURE,
  ID_ROLE_STUDENT,
  routerCategory,
  routerCourses,
  routerDecentralization,
  routerDepartment,
  routerHome,
  routerProject,
  routerProjectAdd,
  routerProjectDetail,
  routerProjectEdit, routerRevise,
  routerStatistical,
  routerSubjects,
  routerUser,
  routerYearsSemesters
} from "../_models/constant";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  routersSystem;
  routerAdmin;
  routerLecture;
  routerStudent;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.initRouter();
    console.log(this.router.url);
    const currentUser = this.authenticationService.getCurrentUser;
    if (currentUser || this.router.url == '/login') {
      console.log(currentUser.roleId);

      // if (this.router.url == routerHome || this.router.url == routerProject || this.router.url == '/login') return true ;
      //
      // if (currentUser.roleId == ID_ROLE_STUDENT ) {
      //   localStorage.removeItem('currentUser');
      //   this.router.navigate(['/login']);
      //   return false;
      // }
      //
      // if (currentUser.roleId == ID_ROLE_LECTURE) {
      //   this.routerLecture.map(obj => {
      //     if (obj == this.router.url) {
      //       return true;
      //     }
      //   });
      // }
      // if (currentUser.roleId == ID_ROLE_ADMIN) return true;
      // localStorage.removeItem('currentUser');
      // this.router.navigate(['/login']);
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

  initRouter() {
    this.routersSystem = [routerHome, routerUser , routerDecentralization , routerDepartment , routerYearsSemesters , routerCategory , routerCourses , routerSubjects , routerStatistical , routerProject , routerProjectDetail , routerProjectAdd , routerProjectEdit , routerRevise ];
    this.routerAdmin = this.routersSystem;
    this.routerLecture = [routerHome, routerUser , routerProject , routerProjectDetail , routerProjectAdd, routerRevise, routerCourses];
    this.routerStudent = [routerProject , routerProjectDetail , routerProjectAdd];
    console.log(this.routerAdmin);
  }
}
