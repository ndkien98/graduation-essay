import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {emailValidator} from '../../../../shared/_models/custom-validators';
import {UserService} from '../../../../shared/_service/user.service';
import {Options} from 'select2';
import {
  BASE_AVARTAR,
  DataConvertSelect2, DES_LOAD_DATA_FORM_SERVER, ERROR_INSERT,
  ERROR_LOAD_DATA,
  ID_ROLE_LECTURE,
  ID_ROLE_STUDENT, SUCCESS
} from '../../../../shared/_models/constant';
import {Subject} from 'rxjs';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {DepartmentService} from '../../../../shared/_service/department.service';
import {Select2OptionData} from 'ng-select2';
import {Lecturer, ObjectCustom, Student, User} from '../../../../shared/_models/user';
import {BaseService} from '../../../../shared/_service/base.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  user: ObjectCustom;
  userFormGroup: FormGroup;
  LECTURE = ID_ROLE_LECTURE;
  STUDENT = ID_ROLE_STUDENT;
  currentFile: File;
  avartar: any;
  gender: any;

  public select2Data: Array<Select2OptionData>; // data chính đổ vào select 2
  public options: Options;                        // option của select 2
  private dataConvert: DataConvertSelect2;

  public onClose: Subject<any>;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    public bsModalRef: BsModalRef,
    private departmentService: DepartmentService,
  ) {
  }

  ngOnInit(): void {
    console.log(this.user);
    this.onClose = new Subject();
    BaseService.checkNullOrUndefine(this.user);
    this.createForm();
    this.setdata();
    this.setDataForSelect2();
  }


  createForm() {
    if (this.user.userAvatarUrl == null) {
      this.avartar = 'assets/admin-dashboard/dist/img/user2-160x160.jpg';
    } else {
      this.avartar = BASE_AVARTAR + this.user.userAvatarUrl;
    }
    this.userFormGroup = this.formBuilder.group({
      username: ['', Validators.required],
      fullname: ['', Validators.required],
      birthDate: ['', Validators.required],
      emailAddress: ['', [Validators.required, emailValidator()]],
      phoneNumber: ['', Validators.required],
      roleName: [''],
      address: ['', Validators.required],
    });
  }

  setdata() {
    try {
      if (this.user.roleId == this.LECTURE) {
        this.userService.getLectureById(this.user.id).subscribe((data: any) => {
            this.user = data;
            this.userFormGroup.patchValue({
                username: this.user.username,
                fullname: this.user.fullName,
                birthDate: this.user.birthDate,
                emailAddress: this.user.emailAddress,
                phoneNumber: this.user.phoneNumber,
                roleName: this.user.roleName,
                address: this.user.departmentCode
              }
            );
          },
          error1 => {
            this.bsModalRef.hide();
            this.onClose.next(ERROR_LOAD_DATA);
          }
        );
      } else if (this.user.roleId == this.STUDENT) {
        this.userService.getStudentById(this.user.id).subscribe((data: any) => {
            this.user = data;
            this.userFormGroup.patchValue({
              username: this.user.username,
              fullname: this.user.fullName,
              birthDate: this.user.birthDate,
              emailAddress: this.user.emailAddress,
              phoneNumber: this.user.phoneNumber,
              roleName: this.user.roleName,
              address: this.user.classCode
            });
          },
          error1 => {
            this.bsModalRef.hide();
            this.onClose.next(ERROR_LOAD_DATA);
          }
        );
      } else {
          this.userService.getUserById(this.user.id).subscribe((data: any) => {
            this.user = data;
            this.userFormGroup.patchValue({
                username: this.user.username,
                fullname: this.user.fullName,
                birthDate: this.user.birthDate,
                emailAddress: this.user.emailAddress,
                phoneNumber: this.user.phoneNumber,
                roleName: this.user.roleName,
                address: 'address'
              });
            },
            error1 => {
              this.bsModalRef.hide();
              this.onClose.next(ERROR_LOAD_DATA);
          });
      }
    } catch (exeption) {
      this.onClose.next(ERROR_LOAD_DATA);
      this.bsModalRef.hide();
    }
    BaseService.checkNullOrUndefine(this.user);
  }

  submit() {
    if (this.user.roleId.toString() == ID_ROLE_LECTURE.toString()) {
      // @ts-ignore
      const lecturer = new Lecturer();
      lecturer.id = this.user.id;
      lecturer.roleId = this.user.roleId;
      lecturer.username = this.userFormGroup.controls.username.value;
      lecturer.fullName = this.userFormGroup.controls.fullname.value;
      lecturer.birthDate = this.userFormGroup.controls.birthDate.value;
      lecturer.gender = this.gender;
      lecturer.birthDate = this.userFormGroup.controls.birthDate.value;
      lecturer.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      lecturer.departmentCode = this.userFormGroup.controls.address.value;
      lecturer.emailAddress = this.userFormGroup.controls.emailAddress.value;
      lecturer.status = 1;
      this.userService.editLecture(lecturer, lecturer.id).subscribe((data: any) => {
        this.editAvatar();
        this.editSuccess();
        }, error1 => {
        this.editError();
        }
      );
    } else if (this.user.roleId.toString() == ID_ROLE_STUDENT) {
      // @ts-ignore
      const student = new Student();
      student.id = this.user.id;
      student.roleId = this.user.roleId;
      student.username = this.userFormGroup.controls.username.value;
      student.fullName = this.userFormGroup.controls.fullname.value;
      student.birthDate = this.userFormGroup.controls.birthDate.value;
      student.gender = this.gender;
      student.birthDate = this.userFormGroup.controls.birthDate.value;
      student.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      student.classCode = this.userFormGroup.controls.address.value;
      student.emailAddress = this.userFormGroup.controls.emailAddress.value;
      student.status = 1;
      console.log(student);
      this.userService.editStudent(student, student.id).subscribe((data: any) => {
        this.editAvatar();
        this.editSuccess();
        }, error1 => {
        this.editError();
        }
      );
    } else if (this.user.roleId == 1) {
      const user = new User();
      user.id = this.user.id;
      user.roleId = this.user.roleId;
      user.username = this.userFormGroup.controls.username.value;
      user.fullName = this.userFormGroup.controls.fullname.value;
      user.birthDate = this.userFormGroup.controls.birthDate.value;
      user.gender = this.gender;
      user.birthDate = this.userFormGroup.controls.birthDate.value;
      user.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      user.emailAddress = this.userFormGroup.controls.emailAddress.value;
      user.status = 1;
      console.log(user);
      this.userService.editUser(user, user.id).subscribe((data: any) => {
        this.editAvatar();
        this.editSuccess();
        }, error1 => {
        this.editError();
        }
      );
    } else {
      this.bsModalRef.hide();
      this.onClose.next(ERROR_LOAD_DATA);
    }
  }

  private setDataForSelect2() {
    this.options = {
      theme: 'classic',
      width: '100%',
      placeholder: 'Chọn tên bộ môn',
      dropdownAutoWidth: true,
    };
    const dataArr = [];
    this.departmentService.getAllDepartment().subscribe((data: any) => {
      data.map(obj => {
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = obj.departmentCode;
          this.dataConvert.text = obj.departmentName;
          dataArr.push(this.dataConvert);
        }, error => {
        this.onClose.next(ERROR_LOAD_DATA);
        this.bsModalRef.hide();
        }
      );
      this.select2Data = dataArr;
    });
  }
  submitFile(event) {
    this.currentFile = event.target.files[0];
    const preview = document.getElementById('viewAvatarEU');
    const render = new FileReader();
    render.onload = function(e) {
      // @ts-ignore
      preview.setAttribute('src', e.target.result);
    };
    render.readAsDataURL(this.currentFile);
  }

  onChangeGender(event: Event) {
    this.gender = (event.target as Element).getAttribute('value');
  }

  private editAvatar() {
    this.userService.editAvartar(this.currentFile,this.user.id).subscribe((data: User) => {
      console.log(data);
    },
      error1 => {
      console.log('edit avatar error');
      this.bsModalRef.hide();
      this.onClose.next(ERROR_INSERT);
      }
    );
  }
  private editSuccess() {
    this.bsModalRef.hide();
    this.onClose.next(SUCCESS);
  }
  private editError() {
    this.bsModalRef.hide();
    this.onClose.next(ERROR_INSERT);
  }
}
