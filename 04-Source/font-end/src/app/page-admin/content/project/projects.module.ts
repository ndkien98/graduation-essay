import {FormsModule, NgModel, ReactiveFormsModule} from '@angular/forms';
import {ProjectComponent} from "./project.component";
import {CommonModule} from "@angular/common";
import {DataTablesModule} from "angular-datatables";
import {NgSelect2Module} from "ng-select2";
import {NgModule} from "@angular/core";
import { DetailProjectComponent } from './detail-project/detail-project.component';
import { EditProjectComponent } from './edit-project/edit-project.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { DeleteProjectComponent } from './delete-project/delete-project.component';
import {RouterModule} from '@angular/router';
import {NgxSummernoteModule} from 'ngx-summernote';

@NgModule({
  declarations: [
    ProjectComponent,
    DetailProjectComponent,
    EditProjectComponent,
    AddProjectComponent,
    DeleteProjectComponent
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    ReactiveFormsModule,
    NgSelect2Module,
    RouterModule,
    FormsModule,
    NgxSummernoteModule
  ]
})
export class ProjectsModule {}
