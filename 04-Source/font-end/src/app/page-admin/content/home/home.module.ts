import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeComponent} from './home.component';
import {StatisticalModule} from '../statistical/statistical.module';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    StatisticalModule
  ],
  exports: [
  ]
})
export class HomeModule { }
