create table departments
(
    id              int auto_increment
        primary key,
    department_code varchar(5)                   not null,
    department_name varchar(100) charset utf8mb4 not null,
    constraint department_code
        unique (department_code)
)
    collate = utf8mb4_general_ci;

INSERT INTO projects_database2.departments (id, department_code, department_name) VALUES (1, 'TH01', 'Khoa học máy tính');
INSERT INTO projects_database2.departments (id, department_code, department_name) VALUES (2, 'TH02', 'Công nghệ phần mềm');
INSERT INTO projects_database2.departments (id, department_code, department_name) VALUES (3, 'TH03', 'Toán - Tin ứng dụng');
INSERT INTO projects_database2.departments (id, department_code, department_name) VALUES (5, 'TH04', 'Bộ môn khoa học dữ liệu');