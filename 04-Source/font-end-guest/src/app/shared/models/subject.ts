export class Subject {
    id: number;
    subjectCode: string;
    subjectName: string;
    departmentCode: string;
    departmentName: string;
}
